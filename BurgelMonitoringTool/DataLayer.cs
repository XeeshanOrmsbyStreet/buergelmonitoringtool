﻿using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BurgelMonitoringTool.Model
{
    public class DataLayer
    {
        private static string connectionStringCompanyData = "Data Source=credithqdbde-production.database.windows.net;Initial Catalog=CompanyDataProduction;Persist Security Info=True;User ID=credithqdbde-production-admin;Password=0JMSaHP9ku2fYzJFpBEFqE4Wuig3T9POlP0IMBwpDVZwJrYQxpYfLTyTDQAwmyjGdR3LYb1CzD6D5413gz5nAxOoxPeCKYJ18Prm";
        private static string connectionStringCreditHQDBDEWebProduction = "Data Source=credithqdbde-production.database.windows.net;Initial Catalog=CreditHQDBDEWebProduction;Persist Security Info=True;User ID=credithqdbde-production-admin;Password=0JMSaHP9ku2fYzJFpBEFqE4Wuig3T9POlP0IMBwpDVZwJrYQxpYfLTyTDQAwmyjGdR3LYb1CzD6D5413gz5nAxOoxPeCKYJ18Prm";

        public static string GetCompanyId(string buergelId)
        {
            string queryString = "SELECT[CompanyId], [Name] FROM[Burgel].[Company] where[BurgelId] = " + "'" + buergelId + "'";

            using (SqlConnection connection =new SqlConnection(connectionStringCompanyData))
            {
                SqlCommand command = new SqlCommand(queryString, connection);
                try
                {
                    connection.Open();
                    SqlDataReader reader = command.ExecuteReader();

                    reader.Read();
                    return reader["CompanyId"].ToString();
                }
                catch (Exception ex)
                {
                    Console.WriteLine(ex.Message);
                }
                return null;
            }
        }

        public static string GetCreditScore(string companyId)
        {
            string queryString = "SELECT [CompanyId],[CreditScore] FROM [Burgel].[CreditIndicator] WHERE [CompanyId] = " + "'" + companyId + "'";

            using (SqlConnection connection = new SqlConnection(connectionStringCreditHQDBDEWebProduction))
            {
                SqlCommand command = new SqlCommand(queryString, connection);
                try
                {
                    connection.Open();
                    SqlDataReader reader = command.ExecuteReader();

                    reader.Read();
                    return reader["CreditScore"].ToString();
                }
                catch (Exception ex)
                {
                    Console.WriteLine(ex.Message);
                }
                return null;
            }
        }

        public static string GetPaymentScore(string companyId)
        {
            string queryString = "SELECT [CompanyId] ,[PaymentExperienceCode] FROM [Burgel].[PaymentIndicator] WHERE [CompanyId] = " + "'" + companyId + "'";

            using (SqlConnection connection = new SqlConnection(connectionStringCreditHQDBDEWebProduction))
            {
                SqlCommand command = new SqlCommand(queryString, connection);
                try
                {
                    connection.Open();
                    SqlDataReader reader = command.ExecuteReader();

                    reader.Read();
                    return reader["PaymentExperienceCode"].ToString();
                }
                catch (Exception ex)
                {
                    Console.WriteLine(ex.Message);
                }
                return null;
            }
        }

        public static bool UpdateScores(string companyId, string newCreditScore, string newPaymentScore)
        {
            string queryString = @"DECLARE @CompanyId UniqueIdentifier, @NewCreditScore float, @NewPaymentScore int
SET @CompanyId = {companyId}
SET @NewCreditScore = {creditscore}
SET @NewPaymentScore = {paymentscore}
UPDATE[Burgel].[CreditIndicator] SET[CreditScore] = @NewCreditScore WHERE[CompanyId] = @CompanyId
UPDATE[Burgel].[PaymentIndicator] SET[PaymentExperienceCode] = @NewPaymentScore WHERE[CompanyId] = @CompanyId";

            queryString = queryString.Replace("{companyId}", "'" + companyId + "'").Replace("{creditscore}", "'" + newCreditScore + "'").Replace("{paymentscore}", "'" + newPaymentScore + "'");

            using (SqlConnection connection = new SqlConnection(connectionStringCreditHQDBDEWebProduction))
            {
                SqlCommand command = new SqlCommand(queryString, connection);
                try
                {
                    connection.Open();
                    command.ExecuteNonQuery();
                }
                catch (Exception ex)
                {
                    Console.WriteLine(ex.Message);
                    return false;
                }
                return true;
            }
        }

        public static List<Tuple<string, string, string>> GetWatchlist(string companyId)
        {
            var watchList = new List<Tuple<string, string, string>>();
            string queryString = "SELECT [UserName],[FirstName],[LastName] FROM [dbo].[User] u JOIN [dbo].[Watchlist] w ON u.AccountId = w.AccountId WHERE w.CompanyId = " + "'" + companyId + "'";

            using (SqlConnection connection = new SqlConnection(connectionStringCreditHQDBDEWebProduction))
            {
                SqlCommand command = new SqlCommand(queryString, connection);
                try
                {
                    connection.Open();
                    SqlDataReader reader = command.ExecuteReader();
                    while (reader.Read())
                    {
                        watchList.Add(new Tuple<string, string, string>(reader[0].ToString(), reader[1].ToString(), reader[2].ToString()));
                    }
                    reader.Close();
                }
                catch (Exception ex)
                {
                    Console.WriteLine(ex.Message);
                    return null;
                }
                return watchList;
            }
        }

        public static void test()
        {
            string connectionString = "Data Source=credithqdbde-production.database.windows.net;Initial Catalog=CreditHQDBDEWebProduction;Persist Security Info=True;User ID=credithqdbde-production-admin;Password=0JMSaHP9ku2fYzJFpBEFqE4Wuig3T9POlP0IMBwpDVZwJrYQxpYfLTyTDQAwmyjGdR3LYb1CzD6D5413gz5nAxOoxPeCKYJ18Prm";

            // Provide the query string with a parameter placeholder.
            //string queryString =
            //    "SELECT ProductID, UnitPrice, ProductName from dbo.products "
            //        + "WHERE UnitPrice > @pricePoint "
            //        + "ORDER BY UnitPrice DESC;";

            string queryString =
                "SELECT * FROM [Burgel].[CreditIndicator]";

            // Specify the parameter value.
            int paramValue = 5;

            // Create and open the connection in a using block. This
            // ensures that all resources will be closed and disposed
            // when the code exits.
            using (SqlConnection connection =
                new SqlConnection(connectionString))
            {
                // Create the Command and Parameter objects.
                SqlCommand command = new SqlCommand(queryString, connection);
                //command.Parameters.AddWithValue("@pricePoint", paramValue);

                // Open the connection in a try/catch block.
                // Create and execute the DataReader, writing the result
                // set to the console window.
                try
                {
                    connection.Open();
                    SqlDataReader reader = command.ExecuteReader();
                    while (reader.Read())
                    {
                        Console.WriteLine("\t{0}\t{1}\t{2}",
                            reader[0], reader[1], reader[2]);
                    }
                    reader.Close();
                }
                catch (Exception ex)
                {
                    Console.WriteLine(ex.Message);
                }
                Console.ReadLine();
            }
        }
    }
}
