﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BurgelMonitoringTool.Model
{
    public class BuergelReport
    {
        public string AgencyNumber { get; set; }
        public string InquiryNumber { get; set; }
        public string ReportNumber { get; set; }
    }
}
