﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BurgelMonitoringTool.Model
{
    public class FinancialSituation
    {
        public string ReportedBuergelId { get; set; }
        public string CreditScore { get; set; }
        public string PaymentScore { get; set; }
    }
}
