﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net;
using System.Text;
using System.Threading.Tasks;
using System.Xml;
using System.Xml.Linq;
using BurgelMonitoringTool.Model;

namespace BurgelMonitoringTool
{
    public class Monitor
    {
        #region
        private static string urlArchive = @"https://www.buergel-online.de/rcs-ws/services/b2b/v1/archive";
        private static string urlReport = @"https://www.buergel-online.de/rcs-ws/services/b2b/v2/reportRetrieval";
        private static string soapEnvelopeArchive = @"<?xml version=""1.0"" encoding=""utf-8""?>
<soapenv:Envelope xmlns:soapenv=""http://schemas.xmlsoap.org/soap/envelope/"" xmlns:arc=""http://archive.v1.b2b.ws.rcs.buergel.de/"">
        <soapenv:Header>
    <wsse:Security soapenv:mustUnderstand=""1"" xmlns:wsse=""http://docs.oasis-open.org/wss/2004/01/oasis-200401-wss-wssecurity-secext-1.0.xsd"" xmlns:wsu=""http://docs.oasis-open.org/wss/2004/01/oasis-200401-wss-wssecurity-utility-1.0.xsd"">
      <wsse:UsernameToken>
        <wsse:Username>09821381</wsse:Username>
        <wsse:Password Type=""wsse:PasswordText"">g9e4ayf1</wsse:Password>
      </wsse:UsernameToken>
    </wsse:Security>
  </soapenv:Header>
   <soapenv:Body>
      <arc:getReportList>
         <criteria maxNumberOfResults=""50"">
            <filter>
               <hideReceivedReports>false</hideReceivedReports>
               <reportType>SUPPLEMENT</reportType>
               <product>1135</product>
            </filter>
         </criteria>
      </arc:getReportList>
   </soapenv:Body>
</soapenv:Envelope>";
        private static string soapEnvelopeReport = @"<?xml version=""1.0"" encoding=""utf-8""?><soapenv:Envelope xmlns:rep=""http://report.v2.b2b.ws.rcs.buergel.de/"" xmlns:soapenv=""http://schemas.xmlsoap.org/soap/envelope/"">
 <soapenv:Header>
 <wsse:Security soapenv:mustUnderstand=""1"" xmlns:wsse=""http://docs.oasis-open.org/wss/2004/01/oasis-200401-wss-wssecurity-secext-1.0.xsd"" xmlns:wsu=""http://docs.oasis-open.org/wss/2004/01/oasis-200401-wss-wssecurity-utility-1.0.xsd"">
     <wsse:UsernameToken>
          <wsse:Username>09821381</wsse:Username>
        <wsse:Password Type=""wsse:PasswordText"">g9e4ayf1</wsse:Password>
     </wsse:UsernameToken>
 </wsse:Security>
 </soapenv:Header>
 <soapenv:Body>
 <rep:getReport>
         <buergelAgencyNumber>{{agencyNumber}}</buergelAgencyNumber>
         <inquiryNumber>{{inquiryNumber}}</inquiryNumber>
         <reportNumber>{{reportNumber}}</reportNumber>
      </rep:getReport>
 </soapenv:Body>
</soapenv:Envelope>";
        #endregion


        /// <summary>
        /// Execute a Soap WebService call
        /// </summary>
        public static void Execute()
        {
            var soapArchiveResponse = SoapRequest(soapEnvelopeArchive, urlArchive);

            var resultArchiveXDoc = XDocument.Parse(soapArchiveResponse);
            var archiveEntries = resultArchiveXDoc.Descendants("archiveEntry");
            var report = new StringBuilder();

            var header = "company Id" + "\t" + "Old Credit Score" + "\t" + "New Credit Score" + "\t" + "Old Payment Score" + "\t" + "New Payment Score" + "\n\r\n\r\n\r";
            report.Append(header);

            Console.WriteLine(header);

            foreach (var archive in archiveEntries)
            {
                IEnumerable<XElement> reportId = archive.Descendants("reportId");
                var buergelReport = new BuergelReport
                {
                    AgencyNumber = reportId.ToList()[0].Element("buergelAgencyNumber").Value,
                    InquiryNumber = reportId.ToList()[0].Element("inquiryNumber").Value,
                    ReportNumber = reportId.ToList()[0].Element("reportNumber").Value
                };

                var soapReportResponse = SoapRequest(soapEnvelopeReport
                    .Replace("{{agencyNumber}}", buergelReport.AgencyNumber).
                    Replace("{{inquiryNumber}}", buergelReport.InquiryNumber).
                    Replace("{{reportNumber}}", buergelReport.ReportNumber), urlReport);

                var resultReportXDoc = XDocument.Parse(soapReportResponse);

                var financialSituationXElement = resultReportXDoc.Descendants("financialSituation");

                var financialSituation = new FinancialSituation
                {
                    ReportedBuergelId = resultReportXDoc.Descendants("reportedBuergelId").ToList()[0].Value
                    ,
                    CreditScore = financialSituationXElement.Descendants("value").ToList()[0].Value
                    ,
                    PaymentScore = financialSituationXElement.Descendants("code").ToList()[0].Value
                };

                var companyId = DataLayer.GetCompanyId(financialSituation.ReportedBuergelId);

                var oldCreditScore = DataLayer.GetCreditScore(companyId);

                var oldPaymentScore = DataLayer.GetPaymentScore(companyId);

                var watchList = DataLayer.GetWatchlist(companyId);

                if (financialSituation.CreditScore != oldCreditScore || financialSituation.PaymentScore != oldPaymentScore)
                {
                    var scores = companyId + "\t" + oldCreditScore + "\t" + financialSituation.CreditScore + "\t" + oldPaymentScore + "\t" + financialSituation.PaymentScore + Environment.NewLine;
                    Console.WriteLine(scores);
                    report.Append(scores);

                    foreach (var watch in watchList)
                    {
                        var watchDetail = "Username: " + watch.Item1 + ", FirstName: " + watch.Item2 + ", LastName: " + watch.Item3 + Environment.NewLine;
                        Console.WriteLine(watchDetail);
                        report.Append(watchDetail );
                    }
                    Console.WriteLine(Environment.NewLine);
                    report.Append(Environment.NewLine);
                    //DataLayer.UpdateScores(companyId, financialSituation.CreditScore, financialSituation.PaymentScore);
                }
            }

            File.WriteAllText(DateTime.Now.ToLongDateString() + ".txt", report.ToString());
        }

        private static string SoapRequest(string soapEnvelop, string url)
        {
            HttpWebRequest request = CreateWebRequest(url);
            XmlDocument soapEnvelopeXml = new XmlDocument();
            soapEnvelopeXml.LoadXml(soapEnvelop);

            using (Stream stream = request.GetRequestStream())
            {
                soapEnvelopeXml.Save(stream);
            }

            using (WebResponse response = request.GetResponse())
            {
                using (StreamReader rd = new StreamReader(response.GetResponseStream()))
                {
                    return rd.ReadToEnd();
                }
            }
        }

        /// <summary>
        /// Create a soap webrequest to [Url]
        /// </summary>
        /// <returns></returns>
        public static HttpWebRequest CreateWebRequest(string url)
        {
            HttpWebRequest webRequest = (HttpWebRequest)WebRequest.Create(url);
            webRequest.Headers.Add(@"SOAP:Action");
            webRequest.ContentType = "text/xml;charset=\"utf-8\"";
            webRequest.Accept = "text/xml";
            webRequest.Method = "POST";
            return webRequest;
        }
    }
}
